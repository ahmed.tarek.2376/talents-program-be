
import org.apache.commons.codec.binary.Base64;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter text to encode");
		String text = sc.nextLine();
		
		String text64 = Base64.encodeBase64String(text.getBytes());
		System.out.println(text64);
		sc.close();
	}
}
