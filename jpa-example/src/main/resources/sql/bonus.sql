#Roles : Software Eng, Mentor, Team Lead

SELECT
  e.*,
  r.name
  
FROM
  employee AS e
  INNER JOIN role AS r ON e.role_id = r.role_id
  
WHERE
  r.name = 'Software Eng'
  AND e.employee_id not in (
    SELECT
      DISTINCT pe.employee_id
    FROM
      project_employees AS pe
  );