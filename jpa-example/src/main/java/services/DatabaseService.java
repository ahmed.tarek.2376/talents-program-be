package services;

import entities.Employee;
import java.util.List;

public interface DatabaseService {

    List<Employee> getEmployeesRole();

    List<Employee> getProjectEmployees(String projectName);

    void addEmployeeToProject(Integer eID, Integer pID);

    List<Employee> getEmptyEmployeesByRole(Integer roleID);
}
