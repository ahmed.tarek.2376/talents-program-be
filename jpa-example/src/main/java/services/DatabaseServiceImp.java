package services;

import entities.Employee;
import entities.Project;

import javax.persistence.EntityManager;
import java.util.List;

public class DatabaseServiceImp implements DatabaseService {

    EntityManager em = MyEntityManager.getEntityManager();

    @Override
    public List<Employee> getEmployeesRole(){
        List<Employee> employees = (List<Employee>) em.createQuery("SELECT e FROM employee e").getResultList();

//        employees.forEach(System.out::println);

        return  employees;

    }

    @Override
    public List<Employee> getProjectEmployees(String projectName){

        List<Employee> employees = em
                .createQuery("SELECT e FROM employee e JOIN e.projects project WHERE project.name = :pname ")
                .setParameter("pname", projectName)
                .getResultList();

//        employees.forEach(System.out::println);

        return  employees;
    }

    @Override
    public void addEmployeeToProject(Integer eID, Integer pID) {

        Employee employee = em.find(Employee.class, eID);
        Project project = em.find(Project.class, pID);

        if(employee!=null && project!=null){
            em.getTransaction().begin();
            employee.addToProject(project);
            em.persist(employee);
            em.getTransaction().commit();
        } else {
            System.out.println("Invalid employee or project ID");
        }
    }

    @Override
    public List<Employee> getEmptyEmployeesByRole(Integer roleID){

        List<Employee> employees = em
                .createQuery("SELECT e FROM employee e LEFT JOIN e.projects project WHERE e.role.id = :rID AND project = null")
                .setParameter("rID", roleID)
                .getResultList();

//        employees.forEach(System.out::println);

        return  employees;
    }
}
