package controller;

import entities.Employee;
import services.DatabaseService;
import services.DatabaseServiceImp;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/employee")
public class EmployeesController {

    private static final DatabaseService dbService = new DatabaseServiceImp();

    @GET
    @Path("/all")
    @Produces("application/json")
    public Response getEmployees(){
        List<Employee> employees = dbService.getEmployeesRole();

        return Response
                .status(Response.Status.OK)
                .entity(employees)
                .build();
    }

    @GET
    @Path("/project/{pName}")
    @Produces("application/json")
    public Response getEmployeesInProject(@PathParam("pName") String pName){
        List<Employee> employees = dbService.getProjectEmployees(pName);

        return Response
                .status(Response.Status.OK)
                .entity(employees)
                .build();
    }

    @GET
    @Path("/{eID}/addToProject/{pID}")
    @Produces("application/json")
    public Response addEmployeeToProject(@PathParam("eID") int eID, @PathParam("pID") int pID){
        dbService.addEmployeeToProject(eID,pID);

        return Response
                .status(Response.Status.OK)
                .build();
    }

    @GET
    @Path("/empty/role/{rID}")
    @Produces("application/json")
    public Response getEmptyEmployeesByRole(@PathParam("rID") int rID){
        List<Employee> employees = dbService.getEmptyEmployeesByRole(rID);

        return Response
                .status(Response.Status.OK)
                .entity(employees)
                .build();
    }
}
