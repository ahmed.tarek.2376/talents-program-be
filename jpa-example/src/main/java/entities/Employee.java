package entities;

import javax.persistence.*;

import java.util.Set;


@Entity(name = "employee")
@Table
public class Employee {

    @Id
    @Column(
            name = "employee_id",
            updatable = false
    )
    private Integer id;

    @Column(
            name = "name",
            nullable = false
    )
    private String name;

    @Column(
            name = "email",
            nullable = false
    )
    private String email;

    @Column(
            name = "phone_number",
            nullable = false
    )
    private String phoneNumber;

    @Column(
            name = "age",
            nullable = false
    )
    private Integer age;

    @Column(
            name = "national_id",
            nullable = false
    )
    private Long nationalId;


    //////////////////////////////////////////
    @ManyToMany(mappedBy = "projectEmployees", targetEntity = Project.class, cascade = CascadeType.PERSIST)
    private Set<Project> projects;


    @OneToMany(mappedBy="manager", targetEntity = Project.class)
    private Set<Project> managingProjects;

    @ManyToOne
    @JoinColumn(name="role_id")
    private Role role;

    /////////////////////////////////////////


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Integer getAge() {
        return age;
    }

    public Long getNationalId() {
        return nationalId;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public Set<Project> getManagingProjects() {
        return managingProjects;
    }

    public Role getRole() {
        return role;
    }

    public void addToProject(Project p){
        this.projects.add(p);
        p.addEmployee(this);
    }

    public Employee(){
    }

    @Override
    public String toString() {
        return "Employee{ " +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", age=" + age +
                ", nationalId=" + nationalId +
                ", role=" + role +
                " }";
    }
}
