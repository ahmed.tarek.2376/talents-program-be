package entities;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.SEQUENCE;


@Entity(name = "Role")
@Table
public class Role {

    @Id
    @Column(
            name = "role_id",
        updatable = false
    )
    private Integer id;

    @Column(
            name = "name",
            nullable = false
    )
    private String name;


    /////////////////////////////////////////
    @OneToMany(mappedBy = "role", targetEntity = Employee.class)
    private Set<Employee>  roleEmployee;

    ////////////////////////////////////////

    public Role() {
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Employee> getRoleEmployee() {
        return roleEmployee;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
