package entities;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "project")
@Table
public class Project {

    @Id
    @Column(
            name = "project_id",
            updatable = false
    )
    private Integer id;

    @Column(
            name = "name",
            nullable = false
    )
    private String name;

    @Column(
            name = "start_date",
            nullable = false
    )
    private Date startDate;

    //////////////////////////////////////////
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "project_employees",
            joinColumns = @JoinColumn(name = "project_id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id"))
    private Set<Employee> projectEmployees;

    @ManyToOne
    @JoinColumn(name="manager_id", referencedColumnName = "employee_id", nullable=false)
    private Employee manager;

    /////////////////////////////////////////////////

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Set<Employee> getProjectEmployees() {
        return projectEmployees;
    }

    public Employee getManager() {
        return manager;
    }

    public void addEmployee(Employee e){
        this.projectEmployees.add(e);
    }

    public Project() {
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", startDate=" + startDate +
                '}';
    }
}
