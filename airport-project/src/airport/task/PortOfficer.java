package airport.task;

public class PortOfficer implements Officer{
    @Override
    public void checkTraveller(HealthDetails h) {
        System.out.println(h.getName() + " reached (Port)");
        h.getDetails(this);
        System.out.println();
    }
}
