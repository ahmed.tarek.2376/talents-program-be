package airport.task;

public class ImmigrationOfficer implements Officer {
    @Override
    public void checkTraveller(HealthDetails h) {
        System.out.println(h.getName() + " reached (Immigration)");
        h.getDetails(this);
        System.out.println();
    }
}
