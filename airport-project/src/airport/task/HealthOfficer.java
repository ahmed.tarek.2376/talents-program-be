package airport.task;

public class HealthOfficer implements Officer {
    @Override
    public void checkTraveller(HealthDetails h) {
        System.out.println(h.getName() + " reached (Health)");
        h.getDetails(this);
        System.out.println();
    }
}
