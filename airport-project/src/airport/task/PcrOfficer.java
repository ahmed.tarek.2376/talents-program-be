package airport.task;

public class PcrOfficer implements Officer{
    @Override
    public void checkTraveller(HealthDetails h) {
        System.out.println(h.getName() + " reached (PCR Test)");
        h.getDetails(this);
        h.setPcrStatus("Negative");
        System.out.println();
    }
}
