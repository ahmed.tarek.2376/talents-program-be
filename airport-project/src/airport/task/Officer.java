package airport.task;

import airport.task.HealthDetails;

public interface Officer {
    void checkTraveller(HealthDetails h);
}
