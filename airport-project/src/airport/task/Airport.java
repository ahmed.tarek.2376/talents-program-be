package airport.task;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Airport {

    PortOfficer portOfficer = new PortOfficer();
    ImmigrationOfficer immigrationOfficer = new ImmigrationOfficer();
    HealthOfficer healthOfficer = new HealthOfficer();
    PcrOfficer pcrOfficer = new PcrOfficer();

    public static List<HealthDetails> getTravellers(){
        return Arrays.asList(
          new HealthDetails("Ahmed",
                  Arrays.asList("Paris", "Rome"),
                  false, true,
                  Collections.<String>emptyList(),
                  Arrays.asList("Paracetamol", "ipBrufen")
          ),
        new HealthDetails("Nouran",
            Arrays.asList("Alexandria", "Berlin"),
            true, false,
            Arrays.asList("Arm Disability"),
            Collections.<String>emptyList())
        );
    }

    public static void main(String[] args) {

        Airport airport = new Airport();

        List<HealthDetails> travellers = getTravellers();

        for(HealthDetails traveller: travellers){
            airport.portOfficer.checkTraveller(traveller);
            airport.healthOfficer.checkTraveller(traveller);
            if(traveller.isHasFever()){
                airport.pcrOfficer.checkTraveller(traveller);
            }
            airport.immigrationOfficer.checkTraveller(traveller);
            System.out.println("~~~~~~~~~~~~~~~");
        }

    }
}
