package airport.task;

import java.util.List;

public class HealthDetails {
    private static int count = 0;
    private int id;
    private String name;
    private List<String> placesVisited;
    private boolean hasFever;
    private boolean hasCough;
    private String pcrStatus;

    private List<String> specialConditions;
    private List<String> medicineAllergies;

    public HealthDetails( String name,
                         List<String> placesVisited, boolean hasFever,
                         boolean hasCough,
                         List<String> specialConditions, List<String> medicineAllergies) {
        this.id = ++count;
        this.name = name;
        this.placesVisited = placesVisited;
        this.hasFever = hasFever;
        this.hasCough = hasCough;
        this.pcrStatus = "None";
        this.specialConditions = specialConditions;
        this.medicineAllergies = medicineAllergies;
    }

    public boolean isHasFever() {
        return hasFever;
    }

    public String getName() {
        return name;
    }

    public void setPcrStatus(String pcrStatus) {
        this.pcrStatus = pcrStatus;
    }

    public void getDetails(Officer officer){
        System.out.println("=== Data Seen ===");
        if(officer instanceof PortOfficer) {
            printPublicDetails();
            return;
        }
        if(officer instanceof ImmigrationOfficer) {
            printImmigrationDetails();
            return;
        }
        if(officer instanceof HealthOfficer) {
            printHealthOfficerDetails();
            return;
        }
        if(officer instanceof PcrOfficer) {
            printPcrOfficerDetails();
        }
    }

    private void printPublicDetails(){
        System.out.println(">Name: " + name);
        System.out.println(">PCR Status: " + pcrStatus);

        if(specialConditions.size()>0){
            System.out.println(">Special Conditions: ");
            for(String condition: specialConditions){
                System.out.println("- " + condition);
            }
        }
    }

    public void printImmigrationDetails(){
        printPublicDetails();

        System.out.println(">Places visited last 14 days:");

        if(placesVisited.size()==0){
            System.out.println("- NONE");
        } else {
            for(String place : placesVisited){
                System.out.println("- " + place);
            }
        }
    }

    private void printHealthOfficerDetails(){
        printPublicDetails();
        printImmigrationDetails();

        System.out.println(">Fever: " + hasFever);
        System.out.println(">Cough: " + hasCough);

        System.out.println(">Medicine Allergies:");
        if(medicineAllergies.size()==0){
            System.out.println("- NONE");
        } else {
            for(String allergy : medicineAllergies){
                System.out.println("- " + allergy);
            }
        }
    }

    private void printPcrOfficerDetails(){
        System.out.println(">Name: " + name);
        System.out.println(">Fever: " + hasFever);
        System.out.println(">Cough: " + hasCough);
    }
}
