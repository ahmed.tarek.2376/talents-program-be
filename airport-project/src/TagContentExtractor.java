import java.util.*;
import java.util.regex.*;

public class TagContentExtractor{
    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());

        String regex = "<(.+)>([^<]+)</\\1>";
        Pattern pattern = Pattern.compile(regex);

        while(testCases>0){
            String line = in.nextLine();

            Matcher matcher = pattern.matcher(line);

            int occurrences = 0;

            while(matcher.find()){
                occurrences++;
                System.out.println(matcher.group(2));
            }

            if(occurrences==0){
                System.out.println("None");
            }

            testCases--;
        }
    }
}




