package validation;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class App {
	
	private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	private static Validator validator = factory.getValidator();
	
	public static boolean validateAppointment(Appointment a) {
		
		Set<ConstraintViolation<Appointment>> violations = validator.validate(a);

		if(violations.size()==0) {
		    System.out.println("Appointment Successfully Validated"); 
			return false;
		}
		
		for (ConstraintViolation<Appointment> violation : violations) {
		    System.out.println("Validation failed: " + violation.getMessage()); 
		}
		return false;
	}
	
	public static void main(String[] args) {
		String name = "Ahmed Tarek";
		int age = 25;
		String address = "Nasr city building 25 behind vodafone st 20";
		String email = "ahmed.tarek@gmail.com";
		String doctorName = "Ibrahim";
		LocalDate date = LocalDate.now().minusDays(1);
		
		Appointment a = new Appointment(name, age, address, email, doctorName, date);
		
		validateAppointment(a);
	}

}
