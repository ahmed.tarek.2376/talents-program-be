package validation;
import java.time.LocalDate;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import javax.validation.constraints.Email;
import javax.validation.constraints.FutureOrPresent;

public class Appointment {
	
    @NotNull(message = "Patient name cannot be null")
    @Size(min = 10, max = 100, message = "Patient name must be between 10 and 100 characters")
	private String name;
    
    @Positive(message = "Age should be a positive number")
    @Max(value = 150, message = "Age should not be greater than 150")
	private int age;
    
    @Pattern(regexp = ".*?[b|B]uilding [0-9]+ .*?([s|s]treet|st) +[a-z0-9]+?", 
    		message = "Address should contain building and street number")
	private String address;
	
	@Email(message = "Email should be valid")
	private String email;
	
    @NotNull(message = "Doctor name cannot be null")
    @Size(min = 10, max = 100, message = "Doctor name must be between 10 and 100 characters")
	private String doctorName;
    
    @FutureOrPresent(message = "Appointment date should not be in the past")
	private LocalDate date;
	
	
	public Appointment(
			 String name,
			int age,
			String address,
			String email,
			String doctorName,
			LocalDate date) 
	{
	this.name = name;
	this.age = age;
	this.address = address;
	this.email = email;
	this.doctorName = doctorName;
	this.date = date;
		
	}
}
