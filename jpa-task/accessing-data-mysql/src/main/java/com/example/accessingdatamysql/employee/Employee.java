package com.example.accessingdatamysql.employee;

import com.example.accessingdatamysql.project.Project;
import com.example.accessingdatamysql.role.Role;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity(name = "employee")
@Table
public class Employee {

    @Id
    @SequenceGenerator(
            name = "employee_sequence",
            sequenceName = "employee_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "employee_sequence"
    )
    @Column(
            name = "employee_id",
            updatable = false
    )
    private Integer id;

    @Column(
            name = "name",
            nullable = false
    )
    private String name;

    @Column(
            name = "email",
            nullable = false
    )
    private String email;

    @Column(
            name = "phone_number",
            nullable = false
    )
    private String phoneNumber;

    @Column(
            name = "age",
            nullable = false
    )
    private Integer age;

    @Column(
            name = "national_id",
            nullable = false
    )
    private Long nationalId;


    //////////////////////////////////////////
    @ManyToMany(mappedBy = "projectEmployees")
    private Set<Project> projects;


    @OneToMany(mappedBy="manager")
    private Set<Project> managingProjects;

    @ManyToOne
    @JoinColumn(name="role_id")
    private Role role;

    /////////////////////////////////////////


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Integer getAge() {
        return age;
    }

    public Long getNationalId() {
        return nationalId;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public Set<Project> getManagingProjects() {
        return managingProjects;
    }

    public Role getRole() {
        return role;
    }

    public Employee(){
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", age=" + age +
                ", nationalId=" + nationalId +
                '}';
    }
}
