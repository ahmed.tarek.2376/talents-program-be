package com.example.accessingdatamysql.repo;

import com.example.accessingdatamysql.employee.Employee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface TaskRepository extends org.springframework.data.repository.Repository<Employee, Integer> {

    @Query(value = "SELECT employee.*, role.name\n" +
            "FROM employee\n" +
            "INNER JOIN role ON employee.role_id=role.role_id",
            nativeQuery = true)
    List<Employee> getEmployeesRole();

    @Query(value = "SELECT\n" +
            "  e.*\n" +
            "FROM\n" +
            "  project_employees AS pe\n" +
            "  INNER JOIN project AS p ON pe.project_id = p.project_id\n" +
            "  INNER JOIN employee AS e ON pe.employee_id = e.employee_id\n" +
            "WHERE p.name = ?1" , nativeQuery = true)
    List<Employee> getEmployeesByProjectName(String projectName);

    @Query(value = "INSERT INTO project_employees\n" +
            "VALUES (?1, ?2)"
            , nativeQuery = true)
    void addEmployeeToProject(Integer employeeID, Integer projectID);

    @Query(value = "SELECT\n" +
            "  e.*,\n" +
            "FROM\n" +
            "  employee AS e\n" +
            "  INNER JOIN role AS r ON e.role_id = ?1\n" +
            "WHERE\n" +
            "  r.name = 'Software Eng'\n" +
            "  AND e.employee_id not in (\n" +
            "    SELECT\n" +
            "      DISTINCT pe.employee_id\n" +
            "    FROM\n" +
            "      project_employees AS pe)"
            , nativeQuery = true)
    List<Employee> getNoProjectWithRole(Integer roleID);
}
