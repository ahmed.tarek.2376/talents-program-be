#Projects: HSBC, Electricity

SELECT
  e.*, p.name AS project_name
  
FROM
  project_employees AS pe
  INNER JOIN project AS p ON pe.project_id = p.project_id
  INNER JOIN employee AS e ON pe.employee_id = e.employee_id
  
WHERE p.name = 'HSBC';