DROP TABLE Project_Employees;
DROP TABLE Project;
DROP TABLE Employee;
DROP TABLE Role;

CREATE TABLE Role (
  role_id int NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NOT NULL,
  PRIMARY KEY (role_id)
);

CREATE TABLE Employee (
  employee_id int NOT NULL AUTO_INCREMENT,
  age TINYINT NOT NULL,
  email VARCHAR(50) NOT NULL,
  name VARCHAR(50) NOT NULL,
  national_id BIGINT NOT NULL UNIQUE,
  phone_number VARCHAR(15) NOT NULL,
  role_id int,
  PRIMARY KEY (employee_id),
  FOREIGN KEY (role_id) REFERENCES Role(role_id)
);

CREATE TABLE Project (
  project_id int NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  start_date DATE NOT NULL,
  manager_id int NOT NULL,
  PRIMARY KEY (project_id),
  FOREIGN KEY (manager_id) REFERENCES Employee(employee_id)
);

CREATE TABLE Project_Employees (
  project_id int NOT NULL,
  employee_id int NOT NULL,
  PRIMARY KEY (project_id, employee_id),
  FOREIGN KEY (project_id) REFERENCES Project(project_id),
  FOREIGN KEY (employee_id) REFERENCES Employee(employee_id)
);