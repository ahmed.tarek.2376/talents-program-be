package services;

import controller.EmployeesController;
import dto.EmployeeDTO;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class DatabaseServiceImpTest {

    List<EmployeeDTO> expectedResult = List.of(
            new EmployeeDTO(1,"name", "email", "phoneNumber", 25, 1234354L)
    );

    @Mock
    private static EntityManager em;
    private DatabaseServiceImp underTest;

    @BeforeAll
    public static void init(){
        Mockito.mockStatic(MyEntityManager.class);
    }

    @BeforeEach
    void setUp() {
        underTest = new DatabaseServiceImp();
    }

    @Test
    void getEmployeesRole() {
        EntityManager em = Mockito.mock(EntityManager.class);
        Mockito.when(MyEntityManager.getEntityManager()).thenReturn(em);

        TypedQuery<EmployeeDTO> query = (TypedQuery<EmployeeDTO>) Mockito.mock(TypedQuery.class);

        Mockito.when(em.createQuery("SELECT new dto.EmployeeDTO(e) FROM employee e")).thenReturn(query);

        Mockito.when(query.getResultList()).thenReturn(expectedResult);

        DatabaseServiceImp dbServiceImp = new DatabaseServiceImp();
        List<EmployeeDTO> actualResult = dbServiceImp.getEmployeesRole();

        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    void getProjectEmployees() {

        Mockito.when(em.createQuery("SELECT new dto.EmployeeDTO(e) FROM employee e JOIN e.projects project WHERE project.name = :pname ")
                .setParameter("pname", "HSBC").getResultList()).thenReturn(expectedResult);

        List<EmployeeDTO> actualResult = underTest.getProjectEmployees("HSBC");

        assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    void addEmployeeToProject() {
    }

    @Test
    void getEmptyEmployeesByRole() {
        int roleID = 1;

        TypedQuery<EmployeeDTO> query = (TypedQuery<EmployeeDTO>) Mockito.mock(TypedQuery.class);
        Mockito.when(query.getResultList()).thenReturn(expectedResult);

        EntityManager em = Mockito.spy(EntityManager.class);
        Mockito.when(em.createQuery("SELECT new dto.EmployeeDTO(e) FROM employee e LEFT JOIN e.projects project WHERE e.role.id = :rID AND project = null")
                        .setParameter("rID", roleID))
                .thenReturn(query);

        Mockito.when(MyEntityManager.getEntityManager()).thenReturn(em);

        underTest = new DatabaseServiceImp();

        List<EmployeeDTO> actualResult = underTest.getProjectEmployees("HSBC");

        assertThat(actualResult).isEqualTo(expectedResult);

    }

    @Test
    void getEmployeesPaginated() {
    }
}