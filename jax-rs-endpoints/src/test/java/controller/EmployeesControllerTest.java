package controller;

import dto.EmployeeDTO;
import dto.PaginationDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import services.DatabaseServiceImp;

import javax.ws.rs.core.Response;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class EmployeesControllerTest {

    @Mock private DatabaseServiceImp dbService;
    private EmployeesController underTest;

    @BeforeEach
    void setUp() {
        underTest = new EmployeesController(dbService);
    }

    @Test
    void getEmployees() {
        List<EmployeeDTO> employees = List.of(
                new EmployeeDTO(1,"name", "email", "phoneNumber", 25, 1234354L)
        );

        DatabaseServiceImp dbService = Mockito.spy(DatabaseServiceImp.class);
        Mockito.doReturn(employees).when(dbService).getEmployeesRole();

        underTest = new EmployeesController(dbService);
        Response response = underTest.getEmployees();
        List<EmployeeDTO> responseEmployees = (List<EmployeeDTO>)response.getEntity();

        assertThat(responseEmployees).contains(employees.get(0));
    }

    @Test
    void getEmployeesInProject() {
        String projectName = "HSBC";

        underTest.getEmployeesInProject(projectName);

        verify(dbService).getProjectEmployees(projectName);
    }

    @Test
    void addEmployeeToProject() {
        int eID = 1;
        int pID = 1;

        underTest.addEmployeeToProject(eID,pID);

        verify(dbService).addEmployeeToProject(eID,pID);
    }

    @Test
    void getEmptyEmployeesByRole() {
        int rID = 2;

        underTest.getEmptyEmployeesByRole(rID);

        verify(dbService).getEmptyEmployeesByRole(rID);
    }

    @Test
    void getEmployeesPaginated() {
        PaginationDTO pagination = new PaginationDTO(2,2);

        underTest.getEmployeesPaginated(pagination);

        verify(dbService).getEmployeesPaginated(pagination.getPageIndex(), pagination.getPageSize());
    }
}