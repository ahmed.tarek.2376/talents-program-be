DELETE FROM
  project_employees;
  
DELETE FROM
  project;
  
DELETE FROM
  employee;
  
DELETE FROM
  role;

  
#########################

INSERT INTO ROLE
VALUES (1, 'Software Eng');

INSERT INTO ROLE
VALUES (2, 'Mentor');

INSERT INTO ROLE
VALUES (3, 'Team Lead');
#########################

INSERT INTO employee (name, age, email, phone_number, national_id, role_id)
VALUES ('Ahmed Tarek', 26, 'atarek@sumerge.com', '+20123456789', 12345678, 1);

INSERT INTO employee (name, age, email, phone_number, national_id, role_id)
VALUES ('Shady Azouz', 25, 'sazouz@sumerge.com', '+20123456789', 65643543, 1);

INSERT INTO employee (name, age, email, phone_number, national_id, role_id)
VALUES ('Nada Araby', 23, 'naraby@sumerge.com', '+20123456789', 5643243, 1);

INSERT INTO employee (name, age, email, phone_number, national_id, role_id)
VALUES ('Gasser Ahmed', 23, 'gahmed@sumerge.com', '+20123456789', 5125155, 1);


INSERT INTO employee (name, age, email, phone_number, national_id, role_id)
VALUES ('Yomna Yousry', 27, 'yyousry@sumerge.com', '+20123456789', 3543465, 2);

INSERT INTO employee (name, age, email, phone_number, national_id, role_id)
VALUES ('Moataz Sakkar', 29, 'msakkar@sumerge.com', '+20123456789', 465463, 2);

INSERT INTO employee (name, age, email, phone_number, national_id, role_id)
VALUES ('Hussain Yahia', 26, 'hyahia@sumerge.com', '+20123456789', 245346, 3);

##########################

INSERT INTO project
Values (1, 'HSBC', '2011-12-18', 6);

INSERT INTO project
Values (2, 'Electricity', '2011-12-18', 6);

###########################

INSERT INTO project_employees
VALUES (1, 1);

INSERT INTO project_employees
VALUES (1, 2);

INSERT INTO project_employees
VALUES (1, 3);

INSERT INTO project_employees
VALUES (1, 5);

INSERT INTO project_employees
VALUES (2, 1);

INSERT INTO project_employees
VALUES (2, 2);

INSERT INTO project_employees
VALUES (2, 3);