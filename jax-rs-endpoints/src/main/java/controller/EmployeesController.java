package controller;

import dto.EmployeeDTO;

import dto.PaginationDTO;
import services.DatabaseService;
import services.DatabaseServiceImp;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/employee")
public class EmployeesController {

//    private static final DatabaseService dbService = new DatabaseServiceImp();
    private DatabaseService dbService;

    public EmployeesController(){
        dbService = new DatabaseServiceImp();
    }

    public EmployeesController(DatabaseService dbService){
        this.dbService = dbService;
    }

    @GET
    @Path("/all")
    @Produces("application/json")
    public Response getEmployees(){
        List<EmployeeDTO> employees = dbService.getEmployeesRole();

        return Response.ok(employees).build();
    }

    @GET
    @Path("/project/{pName}")
    @Produces("application/json")
    public Response getEmployeesInProject(@PathParam("pName") String pName){
        List<EmployeeDTO> employees = dbService.getProjectEmployees(pName);

        return Response.ok(employees).build();
    }

    @PUT
    @Path("/{eID}/addToProject/{pID}")
    @Produces("application/json")
    public Response addEmployeeToProject(@PathParam("eID") int eID, @PathParam("pID") int pID){
        dbService.addEmployeeToProject(eID,pID);

        return Response.ok().build();

    }

    @GET
    @Path("/empty/role/{rID}")
    @Produces("application/json")
    public Response getEmptyEmployeesByRole(@PathParam("rID") int rID){
        List<EmployeeDTO> employees = dbService.getEmptyEmployeesByRole(rID);

        return Response.ok(employees).build();
    }

    @POST
    @Path("/paginated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmployeesPaginated(PaginationDTO body){
        return Response.ok(
                dbService.getEmployeesPaginated(body.getPageIndex(), body.getPageSize())
        ).build();
    }
}
