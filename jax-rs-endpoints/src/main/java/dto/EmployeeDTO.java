package dto;

import entities.Employee;

public class EmployeeDTO {

    private Integer id;

    private String name;

    private String email;

    private String phoneNumber;

    private Integer age;

    private Long nationalId;

    private String roleName;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Integer getAge() {
        return age;
    }

    public Long getNationalId() {
        return nationalId;
    }

    public String getRoleName() {
        return roleName;
    }

    public EmployeeDTO(Integer id, String name, String email, String phoneNumber, Integer age, Long nationalId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.age = age;
        this.nationalId = nationalId;
    }

    public EmployeeDTO(Employee e){
        this.id = e.getId();
        this.name = e.getName();
        this.email = e.getEmail();
        this.phoneNumber = e.getPhoneNumber();
        this.age = e.getAge();
        this.nationalId = e.getNationalId();
        this.roleName = e.getRole().getName();
    }
}
