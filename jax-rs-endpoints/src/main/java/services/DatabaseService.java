package services;

import dto.EmployeeDTO;
import entities.Employee;
import java.util.List;

public interface DatabaseService {

    List<EmployeeDTO> getEmployeesRole();

    List<EmployeeDTO> getProjectEmployees(String projectName);

    void addEmployeeToProject(Integer eID, Integer pID);

    List<EmployeeDTO> getEmptyEmployeesByRole(Integer roleID);

    List<EmployeeDTO> getEmployeesPaginated(Integer pageIndex, Integer pageSize);
}
