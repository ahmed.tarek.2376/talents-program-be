public class Employee {
    private String title;
    private String name;
    private String mobile;

    public Employee(String title, String name, String mobile){
        this.title = title;
        this.name = name;
        this.mobile = mobile;
    }

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    @Override
    public String toString() {
        return "Name: " + name + " - Mobile: " + mobile;
    }
}
