import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamsTask {

    public static List<Employee> getEmployees(){
        return List.of(
                new Employee("SE", "Ahmed Tarek", "01234567"),
                new Employee("SE", "Shady Azouz", "01234567"),
                new Employee("SE", "Nada Araby", "01234567"),
                new Employee("SE", "Sara Maged", "01234567"),
                new Employee("OR", "Yomna Yousry", "01234567"),
                new Employee("Lead", "Hussein Yahya", "01234567"),
                new Employee("ME", "Mohamed Kadry", "01234567"),
                new Employee("ME", "Mohamed Adel", "01234567"),
                new Employee("ME", "Youssef Farahat", "01234567")
        );
    }

    public static void groupEmployees(List<Employee> employees){

        Map<String, List<Employee>> groupedEmployees = employees.stream()
                .collect(Collectors.groupingBy(Employee::getTitle));

        printEmployees(groupedEmployees);
    }

    public static void printEmployees(Map<String, List<Employee>> groupedEmployees){

        groupedEmployees.forEach( (s, employees1) -> {
            if(employees1.size()>1){
                System.out.println("Title " + s + " Count " + employees1.size());
            } else {
                System.out.println("Special Title");
            }
            employees1.forEach(System.out::println);
            System.out.println();
        });

    }

    public static void main(String[] args) {
        groupEmployees(getEmployees());
    }

}
