package com.ahmed.moviesdb.movies.dto;

import lombok.*;

import java.util.List;

@Getter @Setter @ToString
@AllArgsConstructor @NoArgsConstructor
public class MovieUpdateRequest {
    private String language;
    private String releaseDate;
    private List<Integer> genresID;
}
