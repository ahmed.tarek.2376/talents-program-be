package com.ahmed.moviesdb.movies.models;


// to be used for the aggregate query to get movies by flag count
public interface IFlagCount {
    int getMovieId();
    String getTitle();
    int getFlagCount();
}