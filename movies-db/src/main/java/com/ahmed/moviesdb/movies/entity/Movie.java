package com.ahmed.moviesdb.movies.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity @Table @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Getter
public class Movie {

    @Id
    @GeneratedValue
    private Integer id;

    @Setter
    private String title;

    @Column(length = 1000)
    @Size(max = 1000)
    @Setter
    private String overview;
    @Setter
    private String release_date;
    @Setter
    private float popularity;
    @Setter
    private String language;

    @Setter
    private String source;

    @Setter @JsonIgnore
    private boolean active = true;

//    @ManyToMany @Setter
//    @JoinTable(
//            name = "movie_genres",
//            joinColumns = @JoinColumn(name = "movie_id"),
//            inverseJoinColumns = @JoinColumn(name = "genre_id"))
//    private Set<Genre> genres;

    @JsonBackReference
    @OneToMany(mappedBy = "movie")
    @Setter
    private Set<MovieGenre> genres = Set.of();

    @JsonBackReference
    @OneToMany(mappedBy = "movie")
    private Set<MovieRating> ratings = Set.of();

    @JsonBackReference
    @OneToMany(mappedBy = "movie")
    private Set<MovieFlag> flags = Set.of();

    @Transient
    @EqualsAndHashCode.Exclude
    private double averageRating;

    @Transient
    @EqualsAndHashCode.Exclude
    private int rateCount;

    public Movie(String title, String overview, String release_date, float popularity, String source) {
        this.title = title;
        this.overview = overview;
        this.release_date = release_date;
        this.popularity = popularity;
        this.source = source;
    }

    public Movie(Integer id, String title, String overview, String release_date, float popularity, String source) {
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.release_date = release_date;
        this.popularity = popularity;
        this.source = source;

        this.genres = Set.of();
        this.ratings = Set.of();
        this.flags = Set.of();
    }

    public Movie(Integer id, String title, String overview, String language,String release_date, float popularity, String source) {
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.language = language;
        this.release_date = release_date;
        this.popularity = popularity;
        this.source = source;
    }

    public double getAverageRating() {
        return this.ratings.stream()
                .mapToDouble(MovieRating::getRating)
                .average()
                .orElse(0.0);
    }

    public int getRateCount() {
        return this.ratings.size();
    }
}
