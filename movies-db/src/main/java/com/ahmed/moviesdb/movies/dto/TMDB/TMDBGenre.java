package com.ahmed.moviesdb.movies.dto.TMDB;

import com.ahmed.moviesdb.movies.entity.Genre;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.shared.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor @NoArgsConstructor
@Getter @ToString
public class TMDBGenre {
    private Integer id;
    private String name;

    public Genre toGenre(){
        return new Genre(this.id, this.name);
    }
}
