package com.ahmed.moviesdb.movies.service;

import com.ahmed.moviesdb.movies.dto.MovieDTO;
import com.ahmed.moviesdb.movies.entity.Movie;

import java.util.List;

public interface MovieService {
    List<MovieDTO> getAllMoviesActive();
    MovieDTO getMovie(int id);
    List<MovieDTO> getMoviesPaginated(int pIndex, int pSize);

    void flagMovie(int id, String name);
    void rateMovie(int id, String name, int rating);
}
