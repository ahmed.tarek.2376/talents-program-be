package com.ahmed.moviesdb.movies.dto;

import com.ahmed.moviesdb.movies.dto.TMDB.TMDBGenre;
import com.ahmed.moviesdb.movies.entity.*;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.apache.catalina.LifecycleState;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor
@Getter @Setter
public class MovieDTO {
    private Integer id;

    private String title;

    private String overview;

    private String language;

    @JsonProperty("release_date")
    private String releaseDate;

    private float popularity;

    private Set<TMDBGenre> genres;

    private double averageRating;

    private int rateCount;

    public static MovieDTO fromMovieEntity(Movie movie){
        MovieDTO dto = new MovieDTO();
        dto.setId(movie.getId());
        dto.setTitle(movie.getTitle());
        dto.setOverview(movie.getOverview());
        dto.setLanguage(movie.getLanguage());
        dto.setReleaseDate(movie.getRelease_date());
        dto.setPopularity(movie.getPopularity());

        if(movie.getGenres()!=null){
            List<TMDBGenre> genres = movie.getGenres().stream().map(
                    movieGenre -> {
                        return new TMDBGenre(movieGenre.getGenreId(), movieGenre.getGenre().getName());
                    }
            ).collect(Collectors.toList());
            dto.setGenres(Set.copyOf(genres));
        }

        dto.setAverageRating(movie.getAverageRating());
        dto.setRateCount(movie.getRateCount());

        return dto;
    }
}
