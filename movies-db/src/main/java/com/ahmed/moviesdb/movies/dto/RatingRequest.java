package com.ahmed.moviesdb.movies.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor
public class RatingRequest {
    private int rating;
}
