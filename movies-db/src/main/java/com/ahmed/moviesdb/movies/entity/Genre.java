package com.ahmed.moviesdb.movies.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor @NoArgsConstructor @Getter
@Entity @Table(name = "genres")
public class Genre {
    @Id
    private Integer genreId;

    private String name;

    @JsonBackReference
    @OneToMany(mappedBy = "genre")
    private Set<MovieGenre> movies;

    public Genre(Integer genreId, String name) {
        this.genreId = genreId;
        this.name = name;
    }
}
