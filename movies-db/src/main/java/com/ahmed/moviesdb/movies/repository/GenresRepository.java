package com.ahmed.moviesdb.movies.repository;

import com.ahmed.moviesdb.movies.entity.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenresRepository extends JpaRepository<Genre, Integer> {

}
