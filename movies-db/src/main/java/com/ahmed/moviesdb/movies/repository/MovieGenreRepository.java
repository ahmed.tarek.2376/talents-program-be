package com.ahmed.moviesdb.movies.repository;

import com.ahmed.moviesdb.movies.entity.MovieGenre;
import com.ahmed.moviesdb.movies.models.MovieGenreID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieGenreRepository extends JpaRepository<MovieGenre, MovieGenreID> {
    List<MovieGenre> findByMovieId(int mId);
    //TODO to be tested
}
