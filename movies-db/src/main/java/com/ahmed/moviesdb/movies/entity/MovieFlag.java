package com.ahmed.moviesdb.movies.entity;

import com.ahmed.moviesdb.login.entities.User;
import com.ahmed.moviesdb.movies.models.MovieUserID;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity @Table
@NoArgsConstructor @EqualsAndHashCode
@AllArgsConstructor @Getter
@IdClass(MovieUserID.class)
public class MovieFlag {

    @Id
    private Integer userId;
    @Id
    private Integer movieId;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    @JsonManagedReference
    private User user;

    @ManyToOne
    @MapsId("movieId")
    @JoinColumn(name = "movie_id")
    @JsonManagedReference
    private Movie movie;

    public MovieFlag(MovieUserID movieUserID){
        this.userId = movieUserID.getUserId();
        this.movieId = movieUserID.getMovieId();
    }
}
