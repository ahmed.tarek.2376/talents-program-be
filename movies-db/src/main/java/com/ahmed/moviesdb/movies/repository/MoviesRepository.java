package com.ahmed.moviesdb.movies.repository;

import com.ahmed.moviesdb.movies.entity.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MoviesRepository extends JpaRepository<Movie, Integer> {
    Page<Movie> findAll(Pageable pageable);

    List<Movie> findByActive(boolean isActive);

    List<Movie> findAllByActive(boolean isActive, Pageable pageable);
}
