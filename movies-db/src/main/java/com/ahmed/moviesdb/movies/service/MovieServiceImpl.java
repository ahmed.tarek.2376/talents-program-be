package com.ahmed.moviesdb.movies.service;

import com.ahmed.moviesdb.login.repository.UserRepository;
import com.ahmed.moviesdb.movies.dto.MovieDTO;
import com.ahmed.moviesdb.movies.entity.MovieFlag;
import com.ahmed.moviesdb.movies.entity.MovieRating;
import com.ahmed.moviesdb.movies.models.MovieUserID;
import com.ahmed.moviesdb.movies.repository.MovieFlagRepository;
import com.ahmed.moviesdb.movies.repository.MovieRatingRepository;
import com.ahmed.moviesdb.movies.repository.MoviesRepository;
import com.ahmed.moviesdb.movies.entity.Movie;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service @AllArgsConstructor
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MoviesRepository movieRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private MovieFlagRepository movieFlagRepo;

    @Autowired
    private MovieRatingRepository movieRatingRepository;

    @Override
    public List<MovieDTO> getAllMoviesActive() {
        ArrayList<MovieDTO> movies = new ArrayList<>();
        movieRepo.findByActive(true)
                .forEach(movie -> movies.add(MovieDTO.fromMovieEntity(movie)));
        return movies;
    }

    @Override
    public MovieDTO getMovie(int id) {
        Optional<Movie> movie = movieRepo.findById(id);
        if(movie.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No movie with such ID");
        }
        return MovieDTO.fromMovieEntity(movie.get());
    }

    @Override
    public List<MovieDTO> getMoviesPaginated(int pIndex, int pSize) {
        Pageable pagination = PageRequest.of(pIndex, pSize);
        List<MovieDTO> page = movieRepo.findAllByActive(true, pagination)
                .stream().map(MovieDTO::fromMovieEntity).collect(Collectors.toList());
        return page;
    }

    @Override
    public void flagMovie(int id, String username) {
        userRepo.findByUsername(username).ifPresent(
                user -> { // find user
                    movieRepo.findById(id).ifPresentOrElse(
                            movie1 -> { // then find movie by id
                                if(!movie1.isActive()){
                                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not flag a hidden movie");
                                }
                                MovieUserID movieUserID = new MovieUserID(user.getUserId(), movie1.getId());
                                movieFlagRepo.findById(movieUserID).ifPresentOrElse(

                                        movieFlag -> { // if the user already flagged this movie
                                            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Movie already flagged inappropriate by the user");
                                            },

                                        () -> { // if the user did not flag this movie before
                                            movieFlagRepo.save(new MovieFlag(movieUserID));
                                            movieFlagRepo.findByMovieId(movie1.getId()).ifPresent(
                                                    movieFlags -> {
                                                        if(movieFlags.size()>2){ // automatically hide movie if flagged more than 2 times
                                                            movie1.setActive(false);
                                                            movieRepo.save(movie1);
                                                        }
                                                    }
                                            );
                                        }
                                );
                            },
                            () -> { throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No movie with such ID");}
                    );
                }
        );

    }

    @Override
    public void rateMovie(int id, String username, int rating) {

        userRepo.findByUsername(username).ifPresent(
                user -> {
                    movieRepo.findById(id).ifPresentOrElse(
                            movie1 -> {
                                MovieUserID movieUserID = new MovieUserID(user.getUserId(), movie1.getId());
                                movieRatingRepository.findById(movieUserID).ifPresentOrElse(

                                        movieRating -> {
                                            movieRating.setRating(rating);
                                            movieRatingRepository.save(movieRating);
                                        },
                                        () -> movieRatingRepository.save(new MovieRating(movieUserID, rating))
                                );
                            },
                            () -> { throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No movie with such ID");}
                    );
                }
        );
    }


}
