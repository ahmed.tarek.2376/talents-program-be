package com.ahmed.moviesdb.movies.repository;

import com.ahmed.moviesdb.movies.entity.MovieRating;
import com.ahmed.moviesdb.movies.models.MovieUserID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRatingRepository extends JpaRepository<MovieRating, MovieUserID> {

}
