package com.ahmed.moviesdb.movies.controller;

import com.ahmed.moviesdb.movies.dto.MovieDTO;
import com.ahmed.moviesdb.movies.dto.RatingRequest;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.movies.service.MovieService;
import com.ahmed.moviesdb.movies.service.MovieServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("api/movies")
public class MoviesController {

    private MovieService mService;

    public MoviesController(@Autowired MovieServiceImpl m){
        mService=m;
    }

    @GetMapping("/all")
    ResponseEntity<List<MovieDTO>> getAllMovies(){
        return ResponseEntity.ok(mService.getAllMoviesActive());
    }

    @GetMapping("/{id}")
    ResponseEntity<MovieDTO> getMovie(@PathVariable int id){
        return ResponseEntity.ok(mService.getMovie(id));
    }

    @GetMapping()
    ResponseEntity<List<MovieDTO>> getMoviesPaginated(@RequestParam(name = "page") int page,
                                                   @RequestParam(name = "size", defaultValue = "10") int size){

        List<MovieDTO> moviesPaginated = mService.getMoviesPaginated(page, size);
        return ResponseEntity.ok(moviesPaginated);
    }

    @PutMapping("/{id}/flag")
    ResponseEntity<?> flagMovie(@PathVariable int id, Principal principal){
        mService.flagMovie(id, principal.getName());
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}/rate")
    ResponseEntity<?> rateMovie(@PathVariable int id, @RequestBody RatingRequest body, Principal principal){
        if(body.getRating()<0 || body.getRating()>10)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Rating must be between 0 and 10");

        mService.rateMovie(id, principal.getName(), body.getRating());
        return ResponseEntity.ok().build();
    }

}
