package com.ahmed.moviesdb.movies.controller;

import com.ahmed.moviesdb.movies.dto.MovieDTO;
import com.ahmed.moviesdb.movies.dto.MovieUpdateRequest;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.movies.models.IFlagCount;
import com.ahmed.moviesdb.movies.service.AdminMovieService;
import com.ahmed.moviesdb.movies.service.AdminMovieServiceImpl;
import com.ahmed.moviesdb.shared.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/movies")
public class AdminMoviesController {

    private AdminMovieService mService;
    public AdminMoviesController(@Autowired AdminMovieServiceImpl m){
        mService=m;
    }

    @GetMapping("/all")
    ResponseEntity<List<Movie>> getAllMovies(){
        return ResponseEntity.ok(mService.getAllMovies());
    }

    @PutMapping("/hide/{id}")
    ResponseEntity<?> hideMovie(@PathVariable int id){
        mService.hideMovie(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/show/{id}")
    ResponseEntity<?> showMovie(@PathVariable int id){
        mService.showMovie(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping()
    ResponseEntity<MovieDTO> addMovie(@RequestBody Movie movie){
        movie.setSource(Constants.MOVIE_SOURCE_ADMIN);
        Movie createdMovie = mService.saveMovie(movie);
        return ResponseEntity.ok(MovieDTO.fromMovieEntity(createdMovie));
    }

    @PutMapping("/{id}")
    ResponseEntity<?> updateMovie(@PathVariable int id,@RequestBody MovieUpdateRequest movieUpdateRequest){
        mService.updateMovie(id, movieUpdateRequest);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/flagged")
    ResponseEntity<List<IFlagCount>> getFlaggedMovies(){
        List<IFlagCount> flaggedMovies = mService.getFlaggedMovies();
        return ResponseEntity.ok(flaggedMovies);
    }
}
