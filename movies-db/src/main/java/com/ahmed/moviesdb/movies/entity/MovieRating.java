package com.ahmed.moviesdb.movies.entity;

import com.ahmed.moviesdb.login.entities.User;
import com.ahmed.moviesdb.movies.models.MovieUserID;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity @Table
@NoArgsConstructor
@AllArgsConstructor @Getter
@IdClass(MovieUserID.class)
public class MovieRating{

    @Id
    private Integer userId;
    @Id
    private Integer movieId;

    @Setter
    private Integer rating;

    @ManyToOne @MapsId("userId")
    @JoinColumn(name = "user_id")
    @JsonManagedReference
    private User user;

    @ManyToOne @MapsId("movieId")
    @JoinColumn(name = "movie_id")
    @JsonManagedReference
    private Movie movie;

    public MovieRating(MovieUserID movieUserID, Integer rating) {
        this.userId = movieUserID.getUserId();
        this.movieId = movieUserID.getMovieId();
        this.rating = rating;
    }
}
