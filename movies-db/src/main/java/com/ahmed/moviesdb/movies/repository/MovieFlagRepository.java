package com.ahmed.moviesdb.movies.repository;

import com.ahmed.moviesdb.movies.entity.MovieFlag;
import com.ahmed.moviesdb.movies.models.IFlagCount;
import com.ahmed.moviesdb.movies.models.MovieUserID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieFlagRepository extends JpaRepository<MovieFlag, MovieUserID> {

    @Query("SELECT f.movie.id AS movieId, f.movie.title AS title, count(f.movie.id) AS flagCount FROM MovieFlag f GROUP BY f.movie.id")
    List<IFlagCount> findAllByCount();

    Optional<List<MovieFlag>> findByMovieId(int mId);
}
