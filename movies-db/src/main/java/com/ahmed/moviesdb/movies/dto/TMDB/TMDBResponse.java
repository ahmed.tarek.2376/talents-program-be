package com.ahmed.moviesdb.movies.dto.TMDB;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter @RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TMDBResponse {
    private int page;
    private List<TMDBMovie> results;

    @JsonProperty("total_results")
    private int totalResults;

    @JsonProperty("total_pages")
    private int totalPages;
}
