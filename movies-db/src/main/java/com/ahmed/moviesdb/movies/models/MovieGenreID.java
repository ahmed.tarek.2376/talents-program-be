package com.ahmed.moviesdb.movies.models;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;

@NoArgsConstructor @AllArgsConstructor @Getter @EqualsAndHashCode
public class MovieGenreID implements Serializable {

    @Column(name = "movie_id")
    private Integer movieId;

    @Column(name = "genre_id")
    private Integer genreId;
}
