package com.ahmed.moviesdb.movies.service;

import com.ahmed.moviesdb.movies.dto.MovieUpdateRequest;
import com.ahmed.moviesdb.movies.entity.Genre;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.movies.entity.MovieGenre;
import com.ahmed.moviesdb.movies.models.IFlagCount;
import com.ahmed.moviesdb.movies.repository.GenresRepository;
import com.ahmed.moviesdb.movies.repository.MovieFlagRepository;
import com.ahmed.moviesdb.movies.repository.MovieGenreRepository;
import com.ahmed.moviesdb.movies.repository.MoviesRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service @AllArgsConstructor
public class AdminMovieServiceImpl implements AdminMovieService{

    @Autowired
    private MoviesRepository movieRepo;

    @Autowired
    private MovieFlagRepository movieFlagRepository;

    @Autowired
    private GenresRepository genresRepository;

    @Autowired
    private MovieGenreRepository movieGenreRepository;

    @Override
    public List<Movie> getAllMovies() {
        ArrayList<Movie> movies = new ArrayList<Movie>();
        movieRepo.findAll().forEach(movies::add);
        return movies;
    }

    @Override
    public Movie saveMovie(Movie movie) {
        return movieRepo.save(movie);
    }

    @Override
    public void updateMovie(int movieID, MovieUpdateRequest movieUpdateRequest) {
        Optional<Movie> movie = movieRepo.findById(movieID);

        if(movie.isEmpty()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No movie with such ID");
        }

        if(movieUpdateRequest.getGenresID() != null && movieUpdateRequest.getGenresID().size()>0)
            replaceMovieGenres(movieID, movieUpdateRequest.getGenresID());

        //to get the entity object after adding new genres
        Movie movieToUpdate = movieRepo.findById(movieID).get();

        if (movieUpdateRequest.getLanguage() != null)
            movieToUpdate.setLanguage(movieUpdateRequest.getLanguage());
        if(movieUpdateRequest.getReleaseDate() != null)
            movieToUpdate.setRelease_date(movieUpdateRequest.getReleaseDate());

        movieRepo.save(movieToUpdate);
    }

    private void replaceMovieGenres(int movieID, List<Integer> genresID) {
        //check that all genres IDs are valid
        for(int genreID : genresID){
            Optional<Genre> genre = genresRepository.findById(genreID);
            if(genre.isEmpty()){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, genreID + " is not a valid genre id");
            }
        }

        //delete all genres of the movie
        List<MovieGenre> byMovieId = movieGenreRepository.findByMovieId(movieID);
        movieGenreRepository.deleteAll(byMovieId);

        //add new genres to the movie
        for(int genreID : genresID){
            movieGenreRepository.save(new MovieGenre(movieID,genreID));
        }
    }

    @Override
    public void hideMovie(int id) {
        movieRepo.findById(id).ifPresentOrElse(
                movie -> {
                    movie.setActive(false);
                    movieRepo.save(movie);
                },
                () -> {
                    throw new IllegalStateException("No movie with such ID: " + id);
                }
        );
    }

    @Override
    public void showMovie(int id) {
        movieRepo.findById(id).ifPresentOrElse(
                movie -> {
                    movie.setActive(true);
                    movieRepo.save(movie);
                },
                () -> {
                    throw new IllegalStateException("No movie with such ID: " + id);
                }
        );
    }

    @Override
    public void deleteAllMovies(){
        movieRepo.deleteAll();
    }

    @Override
    public long countMovies() {
        return movieRepo.count();
    }

    @Override
    public void saveGenre(Genre genre) {
        genresRepository.save(genre);
    }

    @Override
    public long countGenres() {
        return genresRepository.count();
    }

    @Override
    public void addGenreToMovie(int movieID, int genreID) {
        movieGenreRepository.save(new MovieGenre(movieID, genreID));
    }

    @Override
    public List<IFlagCount> getFlaggedMovies() {
        List<IFlagCount> allByCount = movieFlagRepository.findAllByCount();
        return allByCount;
    }
}
