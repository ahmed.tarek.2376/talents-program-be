package com.ahmed.moviesdb.movies.entity;

import com.ahmed.moviesdb.movies.models.MovieGenreID;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity @Table
@NoArgsConstructor
@AllArgsConstructor @Getter
@IdClass(MovieGenreID.class)
public class MovieGenre {
    @Id
    private Integer movieId;
    @Id
    private Integer genreId;

    @ManyToOne @MapsId("movieId")
    @JoinColumn(name = "movie_id")
    @JsonManagedReference
    private Movie movie;

    @ManyToOne @MapsId("genreId")
    @JoinColumn(name = "genre_id")
    @JsonManagedReference
    private Genre genre;

    public MovieGenre(Integer movieId, Integer genreId) {
        this.movieId = movieId;
        this.genreId = genreId;
    }

}
