package com.ahmed.moviesdb.movies.dto.TMDB;

import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.shared.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Getter @Setter @RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class TMDBMovie {
    private int id;
    private String title;
    private String overview;
    private String release_date;
    private List<Integer> genre_ids;
    public String original_language;
    private float popularity;

    public Movie toMovie(){
        Movie movie = new Movie(id, title, overview.replaceAll("[^\\x00-\\x7F]", ""), original_language,
                release_date, popularity, Constants.MOVIE_SOURCE_TMDB);
        return movie;
    }
}
