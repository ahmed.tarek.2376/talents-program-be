package com.ahmed.moviesdb.movies.service;

import com.ahmed.moviesdb.movies.dto.MovieDTO;
import com.ahmed.moviesdb.movies.dto.MovieUpdateRequest;
import com.ahmed.moviesdb.movies.entity.Genre;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.movies.models.IFlagCount;

import java.util.List;

public interface AdminMovieService {

    Movie saveMovie(Movie movie);
    void updateMovie(int id, MovieUpdateRequest movieUpdateRequest);
    void hideMovie(int id);
    void showMovie(int id);
    void deleteAllMovies();
    long countMovies();
    void saveGenre(Genre genre);
    long countGenres();
    void addGenreToMovie(int movieID, int genreID);

    List<Movie> getAllMovies();

    List<IFlagCount> getFlaggedMovies();
}