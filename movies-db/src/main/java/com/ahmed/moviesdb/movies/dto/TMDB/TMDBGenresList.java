package com.ahmed.moviesdb.movies.dto.TMDB;

import com.ahmed.moviesdb.movies.dto.TMDB.TMDBGenre;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter @Setter @RequiredArgsConstructor @ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class TMDBGenresList {
    private List<TMDBGenre> genres;
}
