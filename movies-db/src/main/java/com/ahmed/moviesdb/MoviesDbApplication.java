package com.ahmed.moviesdb;

import com.ahmed.moviesdb.login.entities.Role;
import com.ahmed.moviesdb.login.entities.User;
import com.ahmed.moviesdb.login.services.UserService;
import com.ahmed.moviesdb.movies.dto.TMDB.TMDBGenre;
import com.ahmed.moviesdb.movies.dto.TMDB.TMDBMovie;
import com.ahmed.moviesdb.movies.dto.TMDB.TMDBGenresList;
import com.ahmed.moviesdb.movies.dto.TMDB.TMDBResponse;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.movies.service.AdminMovieService;
import com.ahmed.moviesdb.shared.Constants;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MoviesDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviesDbApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Profile("!test")
	@Bean
	public CommandLineRunner run(UserService userService, AdminMovieService adminMovieService, RestTemplate restTemplate){
		return args -> {
			userService.startupSaveRole(new Role(1, "ROLE_ADMIN"));
			userService.startupSaveRole(new Role(2, "ROLE_USER"));

			userService.startupSaveUser(new User("ahmedtarek", "password"));
			userService.addRoleToUser("ahmedtarek", "ROLE_ADMIN");

			userService.startupSaveUser(new User("ahmed", "password"));
			userService.addRoleToUser("ahmed", "ROLE_USER");

			userService.startupSaveUser(new User("salma", "password"));
			userService.addRoleToUser("salma", "ROLE_USER");

			userService.startupSaveUser(new User("khaled", "password"));
			userService.addRoleToUser("khaled", "ROLE_USER");

			populateGenres(adminMovieService, restTemplate);
			populateMovies(adminMovieService, restTemplate);
		};
	}

	private void populateGenres(AdminMovieService movieService, RestTemplate restTemplate){
		TMDBGenresList remoteResponse = restTemplate
				.getForObject(
						Constants.GENRES_URL + Constants.REMOTE_API_KEY
						, TMDBGenresList.class);

		if(remoteResponse!=null) {
			if (movieService.countGenres() < 10) {
				for (TMDBGenre TMDBGenre : remoteResponse.getGenres()) {
					movieService.saveGenre(TMDBGenre.toGenre());
				}
			}
		}
	}

	private void populateMovies(AdminMovieService movieService, RestTemplate restTemplate){
		for(int i=1; i<=5; i++){
			TMDBResponse remoteResponse = restTemplate
					.getForObject(
							Constants.POPULAR_MOVIES_URL + Constants.REMOTE_API_KEY + "&page=" + i
							, TMDBResponse.class);
			if(remoteResponse!=null){
				if(movieService.countMovies()<99) {
					for (TMDBMovie TMDBMovie : remoteResponse.getResults()) {
						Movie movie = movieService.saveMovie(TMDBMovie.toMovie());

						for (Integer genreID: TMDBMovie.getGenre_ids()){ // Add movie genres to many to many table
							movieService.addGenreToMovie(movie.getId(), genreID);
						}
					}
				}
			}
		}
	}
}
