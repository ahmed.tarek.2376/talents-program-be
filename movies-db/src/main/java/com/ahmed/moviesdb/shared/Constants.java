package com.ahmed.moviesdb.shared;

public class Constants {
    public static final String POPULAR_MOVIES_URL = "https://api.themoviedb.org/3/movie/popular?api_key=";
    public static final String GENRES_URL = "https://api.themoviedb.org/3/genre/movie/list?api_key=";
    public static final String REMOTE_API_KEY = "127ef8102efbb5a087762506ab8cabc2";

    public static final String MOVIE_SOURCE_TMDB = "TMDB";
    public static final String MOVIE_SOURCE_ADMIN = "ADMIN";

}
