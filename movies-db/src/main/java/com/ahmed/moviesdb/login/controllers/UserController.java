package com.ahmed.moviesdb.login.controllers;

import com.ahmed.moviesdb.login.dto.AuthenticationRequest;
import com.ahmed.moviesdb.login.dto.AuthenticationResponse;
import com.ahmed.moviesdb.login.services.UserService;
import com.ahmed.moviesdb.security.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

@RestController @RequestMapping @RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

//    @GetMapping("/user/{username}")
//    ResponseEntity<User> getUser(@PathVariable String username){
//        return ResponseEntity.ok().body(userService.getUser(username));
//    }

    @PostMapping("/authenticate")
    ResponseEntity<AuthenticationResponse> createAuthenticationToken(@RequestBody AuthenticationRequest body) throws Exception{

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(body.getUsername(), body.getPassword())
            );
        }
        catch (BadCredentialsException e){
            throw new Exception("Invalid username or password", e);
        }
        // if reached here, authentication is successful
        final UserDetails userDetails = userDetailsService.loadUserByUsername(body.getUsername());
        final String jwt = jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }

}
