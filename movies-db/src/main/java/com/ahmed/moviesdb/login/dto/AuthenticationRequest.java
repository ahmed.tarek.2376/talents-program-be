package com.ahmed.moviesdb.login.dto;

import lombok.*;

import java.io.Serializable;

@RequiredArgsConstructor @Getter @Setter
public class AuthenticationRequest implements Serializable {
    private String username;
    private String password;
}
