package com.ahmed.moviesdb.login.services;

import com.ahmed.moviesdb.login.entities.Role;
import com.ahmed.moviesdb.login.entities.User;
import com.ahmed.moviesdb.login.repository.RoleRepository;
import com.ahmed.moviesdb.login.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;


@Service @AllArgsConstructor @Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepo;
    private final RoleRepository roleRepo;

    @Override
    public User saveUser(User newUser) {
        userRepo.findByUsername(newUser.getUsername()).ifPresent( user -> {
            throw new IllegalStateException("Username already exists");
        });

        return userRepo.save(newUser);
    }

    @Override
    public void startupSaveUser(User newUser) {
        userRepo.findByUsername(newUser.getUsername()).ifPresentOrElse(
                user -> {
                    user.setPassword(newUser.getPassword());
                    userRepo.save(user);
                },
                () -> userRepo.save(newUser)
        );
    }

    @Override
    public Role saveRole(Role role) {
        roleRepo.findByName(role.getName()).ifPresent(role1 -> {
            throw new IllegalStateException("Role already exists");
        });

        return roleRepo.save(role);
    }

    @Override
    public void startupSaveRole(Role role) {
        roleRepo.findByName(role.getName()).ifPresentOrElse(
                role1 -> {},
                () -> roleRepo.save(role)
        );
    }


    @Override
    public void addRoleToUser(String username, String roleName) {
        Optional<Role> role = roleRepo.findByName(roleName);
        Optional<User> user = userRepo.findByUsername(username);

        if(role.isEmpty())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No such role name");
        if(user.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user with such username");

        user.get().setRole(role.get());
        userRepo.save(user.get());
    }

    @Override
    public User getUser(String username) {
        Optional<User> user = userRepo.findByUsername(username);

        if(user.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No user with such username");

        return user.get();
    }

}
