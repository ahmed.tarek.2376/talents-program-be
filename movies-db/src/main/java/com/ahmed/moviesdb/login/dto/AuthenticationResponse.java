package com.ahmed.moviesdb.login.dto;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor @Getter
public class AuthenticationResponse implements Serializable {
    private final String token;
}
