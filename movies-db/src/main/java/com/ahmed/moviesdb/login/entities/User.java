package com.ahmed.moviesdb.login.entities;


import com.ahmed.moviesdb.movies.entity.MovieFlag;
import com.ahmed.moviesdb.movies.entity.MovieRating;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity @Table @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Getter
public class User {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Integer userId;
    @Column(unique = true)
    private String username;

    @Setter
    private String password;

    @Setter
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToMany(mappedBy = "user")
    @JsonBackReference
    private Set<MovieRating> ratings;

    @OneToMany(mappedBy = "user")
    @JsonBackReference
    private Set<MovieFlag> flags;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
