package com.ahmed.moviesdb.login.services;

import com.ahmed.moviesdb.login.entities.Role;
import com.ahmed.moviesdb.login.entities.User;

public interface UserService {
    User saveUser(User user);
    Role saveRole(Role role);

    void startupSaveUser(User user);
    void startupSaveRole(Role role);

    void addRoleToUser(String username, String roleName);
    User getUser(String username);
}
