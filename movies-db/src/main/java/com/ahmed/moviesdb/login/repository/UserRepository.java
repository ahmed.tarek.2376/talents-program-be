package com.ahmed.moviesdb.login.repository;

import com.ahmed.moviesdb.login.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String name);
}
