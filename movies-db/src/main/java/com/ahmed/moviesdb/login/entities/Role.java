package com.ahmed.moviesdb.login.entities;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity @Table @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Getter
public class Role {
    @Id @Column(name = "role_id")
    private Integer roleId;

    @Column(unique = true)
    private String name;
}
