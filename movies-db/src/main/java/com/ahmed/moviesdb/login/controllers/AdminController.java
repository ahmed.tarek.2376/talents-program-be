package com.ahmed.moviesdb.login.controllers;

import com.ahmed.moviesdb.login.entities.Role;
import com.ahmed.moviesdb.login.entities.User;
import com.ahmed.moviesdb.login.services.UserService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping("/admin") @RequiredArgsConstructor
public class AdminController {

    private final UserService userService;

    @PostMapping("/user/save")
    ResponseEntity<User> saveUser(@RequestBody User user){
        return ResponseEntity.ok().body(userService.saveUser(user));
    }

    @PostMapping("/role/save")
    ResponseEntity<Role> saveRole(@RequestBody Role role){
        return ResponseEntity.ok().body(userService.saveRole(role));
    }

    @PostMapping("/role/addToUser")
    ResponseEntity<?> addRoleToUser(@RequestBody BodyRoleToUser roleToUser){
        userService.addRoleToUser(roleToUser.getUsername(), roleToUser.getRoleName());
        return ResponseEntity.ok().build();
    }
}

@Getter
@Setter
class BodyRoleToUser{
    private String roleName;
    private String username;
}

@Getter
@Setter
class BodyUser{
    private String username;
    private String password;

    public static User userDtoToUser(BodyUser user){
        return new User(user.getUsername(), user.getPassword());
    }
}
