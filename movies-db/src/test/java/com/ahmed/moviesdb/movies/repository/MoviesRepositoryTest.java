package com.ahmed.moviesdb.movies.repository;

import com.ahmed.moviesdb.movies.entity.Movie;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
class MoviesRepositoryTest {

    @Autowired
    private MoviesRepository underTest;

    private Movie movie1;
    private Movie movie2;
    private Movie movie3;
    private Movie movie4;

    @BeforeEach
    void setUp() {
        this.movie1 = underTest.save(new Movie("test1", "test", "test", 0, "test"));
        this.movie2 = underTest.save(new Movie("test2", "test", "test", 0, "test"));
        this.movie3 = underTest.save(new Movie("test3", "test", "test", 0, "test"));
        this.movie4 = underTest.save(new Movie("test4", "test", "test", 0, "test"));
    }

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void findAll() {
        //given
        movie4.setActive(false);
        underTest.save(movie4);

        //when
        List<Movie> movies = underTest.findAll();

        //then
        assertThat(movies).contains(movie1,movie2,movie3,movie4);
    }

    @Test
    void findByActive() {
        //given
        movie4.setActive(false);
        underTest.save(movie4);

        //when
        List<Movie> movies = underTest.findByActive(true);

        //then
        assertThat(movies).contains(movie1,movie2,movie3)
                .doesNotContain(movie4);
    }

    @Test
    void findAllByActive() {
        //given
        movie4.setActive(false);
        underTest.save(movie4);

        //when
        int pIndex = 0;
        int pSize = 2;
        Pageable pagination = PageRequest.of(pIndex, pSize);
        List<Movie> movies = underTest.findAllByActive(true, pagination);

        //then
        assertThat(movies).contains(movie1,movie2)
                .doesNotContain(movie3,movie4);
    }
}