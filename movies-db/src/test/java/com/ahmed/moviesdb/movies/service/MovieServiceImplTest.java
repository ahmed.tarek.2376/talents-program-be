package com.ahmed.moviesdb.movies.service;

import com.ahmed.moviesdb.login.entities.User;
import com.ahmed.moviesdb.login.repository.UserRepository;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.movies.entity.MovieFlag;
import com.ahmed.moviesdb.movies.entity.MovieRating;
import com.ahmed.moviesdb.movies.models.MovieUserID;
import com.ahmed.moviesdb.movies.repository.MovieFlagRepository;
import com.ahmed.moviesdb.movies.repository.MovieRatingRepository;
import com.ahmed.moviesdb.movies.repository.MoviesRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MovieServiceImplTest {

    private MovieService underTest;

    @Mock private MoviesRepository movieRepo;
    @Mock private UserRepository userRepo;
    @Mock private MovieFlagRepository movieFlagRepo;
    @Mock private MovieRatingRepository movieRatingRepository;

    @BeforeEach
    void setUp() {
        this.underTest = new MovieServiceImpl(movieRepo, userRepo, movieFlagRepo, movieRatingRepository);
    }

    @Test
    void getAllMoviesActive() {
        //when
        underTest.getAllMoviesActive();
        //then
        verify(movieRepo).findByActive(true);
    }

    @Test
    void getMovie() {
        //given
        int id = 1;
        when(movieRepo.findById(id)) //to avoid throwing movie not found exception
                .thenReturn(Optional.of(new Movie()));

        //when
        underTest.getMovie(id);

        //then
        verify(movieRepo).findById(id);
    }

    @Test
    void getMoviesPaginated() {
        //given
        int pIndex = 0;
        int pSize = 2;
        Pageable pagination = PageRequest.of(pIndex, pSize);

        //when
        underTest.getMoviesPaginated(pIndex, pSize);

        //then
        verify(movieRepo).findAllByActive(true, pagination);
    }

    @Test
    void flagMovie() {
        //given
        int movieId = 5;
        int userId = 1;
        String name = "ahmed";

        User user = new User(userId, name, "password", null, null, null);
        Movie movie = new Movie(movieId, "test", "test", "test",0,  "test");
        MovieUserID movieUserID = new MovieUserID(userId, movieId);

        when(userRepo.findByUsername(name)).thenReturn(Optional.of(user));
        when(movieRepo.findById(movieId)).thenReturn(Optional.of(movie));

        //when
        underTest.flagMovie(movieId, name);

        //then
        verify(movieFlagRepo).save(new MovieFlag(movieUserID));
    }

    @Test
    void rateMovie() {
        //given
        int rating = 8;
        int movieId = 5;
        int userId = 1;
        String username = "ahmed";

        User user = new User(userId, username, "password", null, null, null);
        Movie movie = new Movie(movieId, "test", "test", "test",0,  "test");
        MovieUserID movieUserID = new MovieUserID(userId, movieId);

        when(userRepo.findByUsername(username)).thenReturn(Optional.of(user));
        when(movieRepo.findById(movieId)).thenReturn(Optional.of(movie));

        //when
        underTest.rateMovie(movieId,username, rating);

        //then
        ArgumentCaptor<MovieRating> ratingArgumentCaptor =
                ArgumentCaptor.forClass(MovieRating.class);

        verify(movieRatingRepository).save(ratingArgumentCaptor.capture());

        MovieRating capturedRating = ratingArgumentCaptor.getValue();

        assertThat(capturedRating.getMovieId()).isEqualTo(movieId);
        assertThat(capturedRating.getUserId()).isEqualTo(userId);
        assertThat(capturedRating.getRating()).isEqualTo(rating);
    }
}