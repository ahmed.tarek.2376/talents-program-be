package com.ahmed.moviesdb.movies.service;

import com.ahmed.moviesdb.movies.dto.MovieDTO;
import com.ahmed.moviesdb.movies.dto.MovieUpdateRequest;
import com.ahmed.moviesdb.movies.entity.Genre;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.movies.entity.MovieGenre;
import com.ahmed.moviesdb.movies.entity.MovieRating;
import com.ahmed.moviesdb.movies.models.IFlagCount;
import com.ahmed.moviesdb.movies.repository.GenresRepository;
import com.ahmed.moviesdb.movies.repository.MovieFlagRepository;
import com.ahmed.moviesdb.movies.repository.MovieGenreRepository;
import com.ahmed.moviesdb.movies.repository.MoviesRepository;
import org.apache.catalina.LifecycleState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AdminMovieServiceImplTest {

    private AdminMovieService underTest;

    @Mock private MoviesRepository movieRepo;

    @Mock private MovieFlagRepository movieFlagRepository;

    @Mock private GenresRepository genresRepository;

    @Mock private MovieGenreRepository movieGenreRepository;

    @BeforeEach
    void setUp() {
        underTest = new AdminMovieServiceImpl(movieRepo, movieFlagRepository, genresRepository, movieGenreRepository);
    }

    @Test
    void getAllMovies() {
        //when
        underTest.getAllMovies();
        //then
        verify(movieRepo).findAll();
    }

    @Test
    void saveMovie() {
        //given
        Movie movie = new Movie();
        //when
        underTest.saveMovie(movie);
        //then
        verify(movieRepo).save(movie);
    }

    @Test
    void updateMovie() {
        //given
        int movieId = 1;
        Movie movie = new Movie(movieId, "test", "test", "test",0,  "test");
        when(movieRepo.findById(movieId)).thenReturn(Optional.of(movie));

        //when
        underTest.updateMovie(movieId, new MovieUpdateRequest());

        //then
        verify(movieRepo).save(movie);
    }

    @Test
    void hideMovie() {
        //given
        int movieId = 1;
        Movie movie = new Movie(movieId, "test", "test", "test",0,  "test");
        when(movieRepo.findById(movieId)).thenReturn(Optional.of(movie));

        //when
        underTest.hideMovie(movieId);

        //then
        verify(movieRepo).save(movie);
    }

    @Test
    void showMovie() {
        //given
        int movieId = 1;
        Movie movie = new Movie(movieId, "test", "test", "test",0,  "test");
        when(movieRepo.findById(movieId)).thenReturn(Optional.of(movie));

        //when
        underTest.showMovie(movieId);

        //then
        verify(movieRepo).save(movie);
    }

    @Test
    void deleteAllMovies() {
        //when
        underTest.deleteAllMovies();

        //then
        verify(movieRepo).deleteAll();
    }

    @Test
    void countMovies() {
        //when
        underTest.countMovies();

        //then
        verify(movieRepo).count();
    }

    @Test
    void saveGenre() {
        //given
        Genre genre = new Genre();
        //when
        underTest.saveGenre(genre);

        //when
        verify(genresRepository).save(genre);
    }

    @Test
    void countGenres() {
        //when
        underTest.countGenres();

        //then
        verify(genresRepository).count();
    }

    @Test
    void getFlaggedMovies() {
        //when
        underTest.getFlaggedMovies();

        //then
        verify(movieFlagRepository).findAllByCount();
    }

    @Test
    void addGenreToMovie() {
        //when
        int movieID = 1;
        int genreID = 12;
        underTest.addGenreToMovie(movieID, genreID);

        //then
        ArgumentCaptor<MovieGenre> movieGenreArgumentCaptor =
                ArgumentCaptor.forClass(MovieGenre.class);

        verify(movieGenreRepository).save(movieGenreArgumentCaptor.capture());

        MovieGenre capturedMovieGenre = movieGenreArgumentCaptor.getValue();

        assertThat(capturedMovieGenre.getMovieId()).isEqualTo(movieID);
        assertThat(capturedMovieGenre.getGenreId()).isEqualTo(genreID);
    }
}