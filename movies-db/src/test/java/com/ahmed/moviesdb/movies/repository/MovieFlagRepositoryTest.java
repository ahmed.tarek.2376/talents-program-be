package com.ahmed.moviesdb.movies.repository;

import com.ahmed.moviesdb.login.entities.User;
import com.ahmed.moviesdb.login.repository.UserRepository;
import com.ahmed.moviesdb.movies.entity.Movie;
import com.ahmed.moviesdb.movies.entity.MovieFlag;
import com.ahmed.moviesdb.movies.models.IFlagCount;
import com.ahmed.moviesdb.movies.models.MovieUserID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
class MovieFlagRepositoryTest {

    @Autowired
    private MovieFlagRepository underTest;

    @Autowired
    private MoviesRepository moviesRepository;

    @Autowired
    private UserRepository userRepository;

    private Movie movie1;
    private Movie movie2;
    private User user1;
    private User user2;

    @BeforeEach
    void setUp() {
        this.movie1 = moviesRepository.save(new Movie("test1", "test", "test", 0, "test"));
        this.movie2 = moviesRepository.save(new Movie("test2", "test", "test", 0, "test"));
        this.user1 = userRepository.save(new User("ahmed", "password"));
        this.user2 = userRepository.save(new User("salma", "password"));
    }

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void findAllByCount() {
        //given
        MovieFlag m1 = new MovieFlag(new MovieUserID(user1.getUserId(), movie1.getId()));
        MovieFlag m2 = new MovieFlag(new MovieUserID(user2.getUserId(),movie1.getId()));
        MovieFlag m3 = new MovieFlag(new MovieUserID(user1.getUserId(), movie2.getId()));
        underTest.saveAll(List.of(m1,m2,m3));

        //when
        List<IFlagCount> allByCount = underTest.findAllByCount();

        //then
        //TODO Don't know how to test the interface type IFlagCount
    }

    @Test
    void findByMovieId() {
        //given
        MovieFlag m1 = new MovieFlag(new MovieUserID(user1.getUserId(), movie1.getId()));
        MovieFlag m2 = new MovieFlag(new MovieUserID(user2.getUserId(),movie1.getId()));
        MovieFlag m3 = new MovieFlag(new MovieUserID(user1.getUserId(), movie2.getId()));
        underTest.saveAll(List.of(m1,m2,m3));

        //when
        List<MovieFlag> movieFlags = underTest.findByMovieId(movie1.getId()).get();

        //then
        Assertions.assertThat(movieFlags).extracting("movieId")
                .contains(m1.getMovieId(),m2.getMovieId())
                .doesNotContain(m3.getMovieId());
    }
}