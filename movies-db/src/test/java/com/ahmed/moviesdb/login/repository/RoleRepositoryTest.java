package com.ahmed.moviesdb.login.repository;

import com.ahmed.moviesdb.login.entities.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
class RoleRepositoryTest {

    @Autowired
    private RoleRepository underTest;

    private Role role1;
    private Role role2;

    @BeforeEach
    void setUp() {
        this.role1 = underTest.save( new Role(1, "ROLE_ADMIN") );
        this.role2 = underTest.save( new Role(2, "ROLE_USER") );
    }

    @Test
    void findByName() {
        //given

        //when
        String roleName = "ROLE_ADMIN";
        Role role = underTest.findByName(roleName).get();

        //then
        assertThat(role.getName()).isEqualTo(roleName);
    }
}