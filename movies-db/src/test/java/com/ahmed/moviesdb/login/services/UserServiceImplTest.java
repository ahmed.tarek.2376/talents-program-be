package com.ahmed.moviesdb.login.services;

import com.ahmed.moviesdb.login.entities.Role;
import com.ahmed.moviesdb.login.entities.User;
import com.ahmed.moviesdb.login.repository.RoleRepository;
import com.ahmed.moviesdb.login.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private UserService underTest;

    @Mock private UserRepository userRepo;
    @Mock private RoleRepository roleRepo;

    @BeforeEach
    void setUp() {
        underTest = new UserServiceImpl(userRepo, roleRepo);
    }

    @Test
    void saveUser() {
        //given
        User user = new User("ahmed", "password");

        //when
        underTest.saveUser(user);

        //then
        verify(userRepo).save(user);
    }

    @Test
    void startupSaveUser() {
        //given
        User user = new User("ahmed", "password");

        //when
        underTest.startupSaveUser(user);

        //then
        verify(userRepo).save(user);
    }

    @Test
    void saveRole() {
        //given
        Role role = new Role(1, "ROLE_ADMIN");

        //when
        underTest.saveRole(role);

        //then
        verify(roleRepo).save(role);
    }

    @Test
    void startupSaveRole() {
        //given
        Role role = new Role(1, "ROLE_ADMIN");

        //when
        underTest.startupSaveRole(role);

        //then
        verify(roleRepo).save(role);
    }

    @Test
    void addRoleToUser() {
        //given
        String username = "ahmed";
        String roleName = "ROLE_ADMIN";

        User user = new User(username, "password");
        Role role = new Role(1, roleName);

        when(userRepo.findByUsername(username)).thenReturn(Optional.of(user));
        when(roleRepo.findByName(roleName)).thenReturn(Optional.of(role));

        //when
        underTest.addRoleToUser(username, roleName);

        //then
        verify(userRepo).save(user);
    }

    @Test
    void getUser() {
        //given
        String username = "Ahmed";
        User user = new User(username, "password");

        when(userRepo.findByUsername("Ahmed")).thenReturn(Optional.of(user));

        //when
        underTest.getUser(username);

        //then
        verify(userRepo).findByUsername(username);
    }
}