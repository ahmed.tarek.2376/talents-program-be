package com.ahmed.moviesdb.login.repository;

import com.ahmed.moviesdb.login.entities.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

@DataJpaTest
@ActiveProfiles("test")
class UserRepositoryTest {

    @Autowired
    private UserRepository underTest;

    @Test
    void findByUsername() {
        //given
        String username = "ahmed";
        User user = underTest.save(new User(username, "password"));

        //when
        User expected = underTest.findByUsername(username).get();

        //then
        Assertions.assertThat(expected).isEqualTo(user);
    }
}